import ThreeDimensionalShape.Cube;
import ThreeDimensionalShape.Sphere;
import ThreeDimensionalShape.Tetrahedron;
import TwoDimensionalShape.Circle;
import TwoDimensionalShape.Square;
import TwoDimensionalShape.Triangle;
import Shape.Shape;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Tester {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner input = new Scanner(new File("instructor.txt"));
        Shape shape;
        ArrayList<Shape> shapes = new ArrayList<Shape>();


        while (input.hasNextLine()) {
            String line = input.nextLine();
            String[] eleman = line.split(" ");
            if (eleman[0].equals("O") && eleman[1].equals("C")) {
                double radius = Double.parseDouble(eleman[2]);
                shape = new Circle(radius);
                shapes.add(shape);
                System.out.println(shape);

            }else if (eleman[0].equals("O") && eleman[1].equals("S")) {
                double side = Double.parseDouble(eleman[2]);
                shape = new Square(side);
                shapes.add(shape);
                System.out.println(shape);

            }else if (eleman[0].equals("O") && eleman[1].equals("T")) {
                double side1 = Double.parseDouble(eleman[2]);
                double side2 = Double.parseDouble(eleman[3]);
                double side3 = Double.parseDouble(eleman[4]);
                double height = Double.parseDouble(eleman[5]);
                shape = new Triangle(side1, side2, side3, height);
                shapes.add(shape);
                System.out.println(shape);
                
            }else if (eleman[0].equals("O") && eleman[1].equals("SP")) {
                double side = Double.parseDouble(eleman[2]);
                shape = new Sphere(side);
                shapes.add(shape);
                System.out.println(shape);
                
            }else if (eleman[0].equals("O") && eleman[1].equals("CU")) {
                double side = Double.parseDouble(eleman[2]);
                shape = new Cube(side);
                shapes.add(shape);
                System.out.println(shape);

            }else if (eleman[0].equals("O") && eleman[1].equals("TE")) {
                double side = Double.parseDouble(eleman[2]);
                shape = new Tetrahedron(side);
                shapes.add(shape);
                System.out.println(shape);

            }else if (eleman[0].equals("TA")) {
                double totalArea = 0;
                for (int i = 1; i<shapes.size(); i++) {
                    totalArea += shapes.get(i).area();
                }
                System.out.println("Total Area: "+ totalArea);

            }else if (eleman[0].equals("TP")) {
                double totalPerimeter = 0;
                for (int i = 1; i < shapes.size(); i++) {
                    totalPerimeter += shapes.get(i).perimeter();
                }
                System.out.println("Total Perimeter: "+totalPerimeter);
                
            }else if (eleman[0].equals("TV")) {
                double totalVolume = 0;
                for (int i = 1; i < shapes.size(); i++) {
                    totalVolume += shapes.get(i).volume();
                }
                System.out.println("Total Volume: "+totalVolume);
            }
        }
    }
}