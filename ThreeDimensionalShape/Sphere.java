package ThreeDimensionalShape;

public class Sphere extends ThreeDimensionalShape{
	
    protected double radius;

    public Sphere(double radius) {
        this.radius=radius;
    }

    @Override
    public double volume() {
        return (4/3)*Math.PI*Math.pow(radius, 3);

    }

    @Override
    public double area() {
        return (1/3)*Math.PI*Math.pow(radius, 2);
    }

    @Override
    public double perimeter() {
    	return Math.PI*2*this.radius;
    }

}