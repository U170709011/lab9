package ThreeDimensionalShape;

public class Tetrahedron extends ThreeDimensionalShape {

    protected double side;

    public Tetrahedron(double side) {
        this.side = side;

    }

    @Override
    public double area() {
    	 return this.side*this.side*Math.sqrt(3);
    }

    @Override
    public double volume() {
    	 return this.side*this.side*this.side*Math.sqrt(2)/12;
    }

	@Override
	public double perimeter() {
		return side*6;
	}

}