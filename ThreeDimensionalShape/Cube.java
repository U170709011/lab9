package ThreeDimensionalShape;

public class Cube extends ThreeDimensionalShape {

    protected double side;

    public Cube(double side) {
        this.side = side;
    }

    @Override
    public double area() {
        return 6 * Math.pow(this.side, 2);
    }

    @Override
    public double volume() {
        return Math.pow(this.side, 3);
    }
    
    @Override
    public double perimeter() {
    	return 12*this.side;
    }
    
    

}
