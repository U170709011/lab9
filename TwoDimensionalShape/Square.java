package TwoDimensionalShape;

public class Square extends TwoDimensionalShape{
    protected double side;

    public Square(double side) {
        this.side=side;
    }

    @Override
    public double area() {
        return Math.pow(this.side, 2);
    }
    
    @Override
    public double perimeter() {
    	return 4 * this.side; 
    }

}