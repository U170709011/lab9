package TwoDimensionalShape;

public class Circle extends TwoDimensionalShape {
    protected double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double area() {
        return Math.PI * Math.pow(this.radius, 2);
    }
    
    @Override
    public double perimeter() {
    	return Math.PI * 2 * this.radius;
    }

}