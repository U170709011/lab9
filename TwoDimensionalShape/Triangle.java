package TwoDimensionalShape;

public class Triangle extends TwoDimensionalShape{
    protected double side1,side2,side3,height;

    public Triangle(double side1,double side2,double side3,double height) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        this.height = height;
    }

    @Override
    public double area() {
    	double s = (this.side1 + this.side2+ this.side3) / 2;
	    return Math.sqrt( s* (s-this.side1) * (s-this.side2) * (s-this.side3) );
    }
    
    @Override
    public double perimeter() {
    	return side1 + side2 + side3;
    }


}